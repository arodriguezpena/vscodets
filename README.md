## Configuración VSCode

Se debe instalar las siguientes extensiones en VSCODE

- ESLint
- Prettier - Code Formatter
- Typescript Hero
- Bracket Pair Colorizer

## Instalación

```bash
yarn install
```
